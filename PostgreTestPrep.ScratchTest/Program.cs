﻿using System;
using PostgreTestPrep;

namespace PostgreTestPrep.ScratchTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var SqlLoader = new SqlLoader();

            Console.WriteLine("Setup Complete: {0}", SqlLoader.Setup("PersonTest"));

            Console.Read();

            Console.WriteLine("Teardown Complete: {0}", SqlLoader.TearDown("PersonTest"));

            Console.Read();
        }
    }
}
