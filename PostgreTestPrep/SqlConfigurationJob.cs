﻿// <copyright file="SqlConfigurationJob.cs" company="Cinema Coup">
//     Copyright (c) Cinema Coup All rights reserved.
// </copyright>

namespace PostgreTestPrep
{
    /// <summary>
    /// The SQL Configuration Job Class 
    /// </summary>
    internal class SqlConfigurationJob
    {
        /// <summary>
        /// Gets or sets the Before Test SQL File name
        /// </summary>
        public string BeforeTestSqlFile { get; set; }

        /// <summary>
        /// Gets or sets the After Test SQL File name
        /// </summary>
        public string AfterTestSqlFile { get; set; }

        /// <summary>
        /// Gets or sets the Sort Order
        /// </summary>
        public int SortOrder { get; set; }
    }
}