﻿// <copyright file="Settings.cs" company="Cinema Coup">
//     Copyright (c) Cinema Coup All rights reserved.
// </copyright>

namespace PostgreTestPrep
{
    using System.Collections.Generic;
    using System.IO;
    using Newtonsoft.Json;

    /// <summary>
    /// The Settings Class
    /// </summary>
    public class Settings
    {
        #region Public

        /// <summary>
        /// Gets the list of SQL Configurations
        /// </summary>
        internal static List<SqlConfiguration> SqlConfigurations
        {
            get
            {
                if (Settings.sqlConfigurations == null)
                {
                    Settings.LoadSettings();
                }

                return Settings.sqlConfigurations;
            }
        }

        #endregion

        #region Private Properties

        /// <summary>
        /// Gets or sets the Test Database Connection String
        /// </summary>
        private static List<SqlConfiguration> sqlConfigurations { get; set; }

        #endregion

        #region Private Methods

        /// <summary>
        /// Loads the settings from the configuration file
        /// </summary>
        private static void LoadSettings()
        {
            if (!File.Exists("SqlConfiguration.json"))
            {
                throw new FileNotFoundException("The specfied SQL Configuration file was not found");
            }

            using (StreamReader file = File.OpenText("SqlConfiguration.json"))
            {
                var json = file.ReadToEnd();
                Settings.sqlConfigurations = JsonConvert.DeserializeObject<List<SqlConfiguration>>(json);
            }
        }

        #endregion
    }
}