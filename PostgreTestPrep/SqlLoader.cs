﻿// <copyright file="SqlLoader.cs" company="Cinema Coup">
//     Copyright (c) Cinema Coup All rights reserved.
// </copyright>

namespace PostgreTestPrep
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using Exceptions;
    using Npgsql;

    /// <summary>
    /// The SQL Loader class
    /// </summary>
    public class SqlLoader
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlLoader"/> class
        /// </summary>
        public SqlLoader()
        {
            this.SqlConfigurations = Settings.SqlConfigurations;
        }

        #endregion

        #region Private Properties

        /// <summary>
        /// Gets a list of <see cref="SqlConfiguration"/> instances
        /// </summary>
        private List<SqlConfiguration> SqlConfigurations { get; }
        
        #endregion

        #region Public Methods

        /// <summary>
        /// Runs the SQL file to setup the database for a test
        /// </summary>
        /// <param name="sqlConfigurationName">The name of the configuration to be run</param>
        /// <returns>True if successful</returns>
        public bool Setup(string sqlConfigurationName)
        {
            var sqlConfiguration = this.GetSqlConfiguration(sqlConfigurationName);

            var sortedSqlConfigurationJobs = sqlConfiguration.SqlConfigurationJobs.OrderBy(c => c.SortOrder);

            foreach (var sqlConfigurationJob in sortedSqlConfigurationJobs)
            {
                var result = this.ExecuteSqlFile(sqlConfigurationJob.BeforeTestSqlFile, sqlConfiguration.ConnectionString);
            }

            return true;
        }

        /// <summary>
        /// Runs the SQL file to reset the database after a test
        /// </summary>
        /// <param name="sqlConfigurationName">The name of the configuration to be run</param>
        /// <returns>True if successful</returns>
        public bool TearDown(string sqlConfigurationName)
        {
            var sqlConfiguration = this.GetSqlConfiguration(sqlConfigurationName);

            var sortedSqlConfigurationJobs = sqlConfiguration.SqlConfigurationJobs.OrderByDescending(c => c.SortOrder);

            foreach (var sqlConfigurationJob in sortedSqlConfigurationJobs)
            {
                var result = this.ExecuteSqlFile(sqlConfigurationJob.AfterTestSqlFile, sqlConfiguration.ConnectionString);
            }

            return true;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get the loaded SQL Configurations to see of the configuration specified exists
        /// </summary>
        /// <param name="sqlConfigurationName">The name of the SQL test</param>
        /// <returns>True if the SQL Configuration exists</returns>
        private SqlConfiguration GetSqlConfiguration(string sqlConfigurationName)
        {
            var sqlConfiguration = this.SqlConfigurations.First(c => c.SqlConfigurationName == sqlConfigurationName);

            if (sqlConfiguration == null)
            {
                throw new SqlConfigurationNotFoundException(string.Format("No Configuration was found for SQL Test: {0}", sqlConfigurationName));
            }

            return sqlConfiguration;
        }

        /// <summary>
        /// Opens a SQL file and executes the statements
        /// </summary>
        /// <param name="filePath">The path to the SQL File</param>
        /// <returns>The return value from the database call</returns>
        private int ExecuteSqlFile(string filePath, string connectionString)
        {
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException("The specfied SQL Configuration Job file was not found", filePath);
            }

            var sql = File.ReadAllText(filePath);

            using (var connection = new NpgsqlConnection(connectionString))
            {
                using (var command = new NpgsqlCommand(sql, connection))
                {
                    command.CommandType = CommandType.Text;

                    connection.Open();
                    var count = command.ExecuteNonQuery();

                    return count;
                }
            }
        }

        #endregion
    }
}