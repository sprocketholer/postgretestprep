﻿// <copyright file="SqlConfigurationNotFoundException.cs" company="Cinema Coup">
//     Copyright (c) Cinema Coup All rights reserved.
// </copyright>

namespace PostgreTestPrep.Exceptions
{
    using System;

    /// <summary>
    /// The SQL Configuration Not Found Exception class
    /// </summary>
    public class SqlConfigurationNotFoundException : Exception
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlConfigurationNotFoundException"/> class
        /// </summary>
        public SqlConfigurationNotFoundException() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlConfigurationNotFoundException"/> class
        /// </summary>
        /// <param name="message">The exception message</param>
        public SqlConfigurationNotFoundException(string message) : base(message)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SqlConfigurationNotFoundException"/> class
        /// </summary>
        /// <param name="message">The exception message</param>
        /// <param name="innertException">An instance of <see cref="Exception"/></param>
        public SqlConfigurationNotFoundException(string message, Exception innertException) 
            : base(message, innertException)
        {
        }

        #endregion
    }
}