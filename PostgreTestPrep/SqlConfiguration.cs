﻿// <copyright file="SqlConfiguration.cs" company="Cinema Coup">
//     Copyright (c) Cinema Coup All rights reserved.
// </copyright>

namespace PostgreTestPrep
{
    using System.Collections.Generic;

    /// <summary>
    /// The SQL Configuration Class
    /// </summary>
    internal class SqlConfiguration
    {
        /// <summary>
        /// Gets or sets the SQL Configuration Name
        /// </summary>
        public string SqlConfigurationName { get; set; }

        /// <summary>
        /// Gets or sets the connection string
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Gets or sets a list of <see cref="SqlConfigurationJob"/>
        /// </summary>
        public List<SqlConfigurationJob> SqlConfigurationJobs { get; set; }
    }
}